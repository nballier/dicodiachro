# DicoDiachro
---
Corpus and tools for diachronic analysis of english pronunciation based on dictionaries (XVIIIe - XXIe)

Corpus et outils pour études diacrhoniques de la prononciation en langue anglaise basée dictionnaires (XVIIIè - XXIè)

# Table of content


* Buchanan dictionary (1766)
    * Parsing-Normalization stage [notebook]
        * [viewer](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mshs-poitiers/forellis/dicodiachro/raw/master/Buchanan_eng.ipynb)
        * [executable environement](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmshs-poitiers%2Fforellis%2Fdicodiachro/204b3bcd4e4f9826fd9e77035207e9d31c9f20ce?filepath=Buchanan_eng.ipynb) (please wait 3 minutes during server starting)

* Walker dictionary (1809)
    * Parsing-Normalization stage [notebook]
        * viewer
        * executable environement

* Analysis
    * Synerisis
        * Cross comparison Buchanan-Walker  [notebook]
            * viewer
            * executable environement